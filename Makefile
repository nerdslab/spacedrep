CXX=g++
CPPFLAGS=-g -Wall -std=c++1y -O2

all: main

main: src/main.cpp
	mkdir -p ./bin/
	$(CXX) $(CPPFLAGS) src/main.cpp -o bin/spacedrep

.PHONY: clean
clean:
	rm -rf build/
	rm -rf bin/
